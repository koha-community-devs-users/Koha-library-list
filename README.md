Submit your pull requests here to be included on https://koha-community.org/about/libraries 

The list is tab delimited (not 4 spaces!), please put in your library in the order specified by the heading. Provide as much or as little information as you want.

Additionally, if you are a library that uses Koha, you should consider enabling reporting of your installation automatically to http://hea.koha-community.org/
